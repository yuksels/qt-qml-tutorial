// usermanager.h

#ifndef USERMANAGER_H
#define USERMANAGER_H

#include <QObject>

class UserManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString ad READ ad WRITE setAd NOTIFY adChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)

public:
    explicit UserManager(QObject *parent = nullptr);

    QString ad() const;
    QString email() const;
    QString password() const;

public slots:
    void setAd(const QString &ad);
    void setEmail(const QString &email);
    void setPassword(const QString &password);
    void printUserInfo();

signals:
    void adChanged();
    void emailChanged();
    void passwordChanged();

private:
    QString m_ad;
    QString m_email;
    QString m_password;
};

#endif // USERMANAGER_H
