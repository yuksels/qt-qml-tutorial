import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

ApplicationWindow {
    visible: true
    width: 400
    height: 300
    title: "Kullanıcı Kayıt Ekranı"

    Rectangle {
        anchors.fill: parent
        color: "#f0f0f0"

        ColumnLayout {
            anchors.centerIn: parent
            spacing: 10

            Label {
                text: "Ad:"
            }

            TextField {
                id: adField
                placeholderText: "Adınızı girin"
                width: 200
            }

            Label {
                text: "E-posta:"
            }

            TextField {
                id: emailField
                placeholderText: "E-posta adresinizi girin"
                width: 200
            }

            Label {
                text: "Şifre:"
            }

            TextField {
                id: passwordField
                placeholderText: "Şifrenizi girin"
                width: 200
                echoMode: TextInput.Password
            }

            Button {
                text: "Kayıt Ol"
                onClicked: {
                    // Kullanıcı kayıt işlemleri burada yapılacak
                    var ad = adField.text
                    var email = emailField.text
                    var password = passwordField.text
                    userManager.ad = adField.text;
                    userManager.email = emailField.text;
                    userManager.password = passwordField.text;
                    // console.log("Ad: " + ad)
                    // console.log("E-posta: " + email)
                    // console.log("Şifre: " + password)
                    userManager.printUserInfo();
                }
            }
        }
    }
}
