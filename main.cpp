#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "usermanager.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    // UserManager sınıfını QML tarafında erişilebilir kılma
    UserManager userManager;
    engine.rootContext()->setContextProperty("userManager", &userManager);

    // QObject::connect() çağrısını kullanarak hata kontrolü
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);

    // QML dosyasını yükler
    engine.load(QUrl(QStringLiteral("qrc:/qt/qml/Test/Main.qml")));

    return app.exec();
}
