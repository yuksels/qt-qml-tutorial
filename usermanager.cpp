// usermanager.cpp

#include "usermanager.h"
#include <QDebug>

UserManager::UserManager(QObject *parent) : QObject(parent)
{

}

QString UserManager::ad() const
{
    return m_ad;
}

QString UserManager::email() const
{
    return m_email;
}

QString UserManager::password() const
{
    return m_password;
}

void UserManager::setAd(const QString &ad)
{
    qDebug()<< "ad setlendi";
    if (m_ad == ad)
        return;

    m_ad = ad;
    emit adChanged();
}

void UserManager::setEmail(const QString &email)
{
    if (m_email == email)
        return;

    m_email = email;
    emit emailChanged();
}

void UserManager::setPassword(const QString &password)
{
    if (m_password == password)
        return;

    m_password = password;
    emit passwordChanged();
}

void UserManager::printUserInfo()
{
    qDebug() << "Ad: " << m_ad;
    qDebug() << "E-posta: " << m_email;
    qDebug() << "Şifre: " << m_password;
}
